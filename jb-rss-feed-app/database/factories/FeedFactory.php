<?php

namespace Database\Factories;

use App\Models\Feed;
use Illuminate\Database\Eloquent\Factories\Factory;

class FeedFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Feed::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
       
            return [
                'title' => $this->faker->sentence,
                'description' => "<p>".implode("</p>\n\n<p>", $this->faker->paragraphs(rand(3,6)))."</p>",
                'siteTitle' => $this->faker->name,
                'link' => $this->faker->text,
                'user_id' => \App\Models\User::factory(),
                'image' => $this->faker->text,
                'favicon' => $this->faker->text,
                


            ];
    }    

}
