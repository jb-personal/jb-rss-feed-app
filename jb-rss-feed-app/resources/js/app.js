require('./bootstrap');

import Vue from 'vue'
import Login from './components/auth/LoginComponent'
import Register from './components/auth/RegisterComponent'
import Dashboard from './components/DashboardComponent'
import VueMaterial from 'vue-material'
import VueRouter from 'vue-router'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'
import '../../public/css/app.css'

Vue.use(VueMaterial)

window.Vue = require('vue').default;



Vue.use(VueRouter)

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: Login,
            props: true
        },
        {
            path: '/register',
            name: 'Register',
            component: Register,
            props: true
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: Dashboard,
            props: true
        },
    ],
});


Vue.component('Login', require('./components/auth/LoginComponent.vue').default);
Vue.component('Register', require('./components/auth/RegisterComponent.vue').default);
Vue.component('Dashboard', require('./components/DashboardComponent.vue').default);


const app = new Vue({
    el: '#app',
    router,
    components: { Login, Register, Dashboard },
});
