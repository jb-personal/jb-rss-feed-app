<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Welcome to RSS Reader</title>
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:400,500,700,400italic|Material+Icons">
    <style>
        html,
        body {
            background-color: #fafafa;
            color: #353535;
            font-family: "Roboto";
            font-size: 16px;
            font-smoothing: antialiased;
            overflow: hidden;
        }

    </style>
</head>

<body>
    <div id="app">
        <Login></Login>
    </div>
    <script src="{{ asset('js/app.js') }}"></script>

</body>

</html>
