<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addRSSFeedUrl(Request $request) {
        
        $userDetails = Auth::user();  // To get the logged-in user details
        $user = User::find($userDetails ->id);  // Find the user using model and hold its reference
        $user->rss_feed_url=$request->input('rss_feed_url');



        $user->save();

      }
      public function getUserDetails(User $user) {
        
        $userDetails = Auth::user(); 
        $user = User::find($userDetails ->id);

        return $user;
      }
}
